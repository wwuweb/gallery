<?php
/**
 * @file
 * gallery.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function gallery_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'galleries';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Galleries';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Galleries';
  $handler->display->display_options['css_class'] = 'wwu-galleries';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '6';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '5';
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['style_options']['row_class'] = 'wwu-galleries-row';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['columns'] = '2';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = '0';
  $handler->display->display_options['fields']['title']['element_label_type'] = '0';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_wrapper_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_wrapper_class'] = 'wwu-galleries-title';
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Gallery Photographer */
  $handler->display->display_options['fields']['field_gallery_photographer']['id'] = 'field_gallery_photographer';
  $handler->display->display_options['fields']['field_gallery_photographer']['table'] = 'field_data_field_gallery_photographer';
  $handler->display->display_options['fields']['field_gallery_photographer']['field'] = 'field_gallery_photographer';
  $handler->display->display_options['fields']['field_gallery_photographer']['label'] = 'Photographed by';
  $handler->display->display_options['fields']['field_gallery_photographer']['element_type'] = '0';
  $handler->display->display_options['fields']['field_gallery_photographer']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_gallery_photographer']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_gallery_photographer']['element_wrapper_type'] = 'h3';
  $handler->display->display_options['fields']['field_gallery_photographer']['element_wrapper_class'] = 'wwu-galleries-photographer';
  $handler->display->display_options['fields']['field_gallery_photographer']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_gallery_photographer']['empty'] = 'Anonymous';
  $handler->display->display_options['fields']['field_gallery_photographer']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_gallery_photographer']['type'] = 'text_plain';
  $handler->display->display_options['fields']['field_gallery_photographer']['delta_offset'] = '0';
  /* Field: Content: Gallery Images */
  $handler->display->display_options['fields']['field_gallery_images']['id'] = 'field_gallery_images';
  $handler->display->display_options['fields']['field_gallery_images']['table'] = 'field_data_field_gallery_images';
  $handler->display->display_options['fields']['field_gallery_images']['field'] = 'field_gallery_images';
  $handler->display->display_options['fields']['field_gallery_images']['label'] = '';
  $handler->display->display_options['fields']['field_gallery_images']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_gallery_images']['element_wrapper_class'] = 'wwu-galleries-images';
  $handler->display->display_options['fields']['field_gallery_images']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_gallery_images']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_gallery_images']['type'] = 'colorbox';
  $handler->display->display_options['fields']['field_gallery_images']['settings'] = array(
    'colorbox_node_style' => 'gallery_thumbnail',
    'colorbox_node_style_first' => '',
    'colorbox_image_style' => 'gallery_full_size',
    'colorbox_gallery' => 'field_post',
    'colorbox_gallery_custom' => '',
    'colorbox_caption' => 'none',
    'colorbox_caption_custom' => '',
  );
  $handler->display->display_options['fields']['field_gallery_images']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_gallery_images']['separator'] = '';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'gallery' => 'gallery',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'galleries';
  $export['galleries'] = $view;

  return $export;
}
